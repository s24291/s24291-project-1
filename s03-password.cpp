#include <iostream>
#include <string>

auto main(int argc, char*argv[]) -> int
{ 
    auto const password = std::string{argv[1]};
    auto guessed_password = std::string{};
do
{
    std::cout << "password:";
    std::getline(std::cin, guessed_password);
}
while (guessed_password != password);

    std::cout << "OK!" << std::endl;

return 0;
}

