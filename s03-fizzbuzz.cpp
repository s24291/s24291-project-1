#include <iostream>
using namespace std;

int main (int argc, char*argv[]) 
{
    auto const a= stoi(argv[1]);

    for (auto i=1; i <= a; i++)
    {
        if (i%15 == 0)
        {
            cout << "FizzBuzz" << "\n";
        }
        
        else if (i%3 == 0)
        {
            cout << "Fizz" << "\n";
        }
        else if (i%5 == 0)
        {
            cout << "Buzz" << "\n";
        }
        
        else
        {
            cout << i << "\n";
        }
    }
    
    return 0;
}
    
