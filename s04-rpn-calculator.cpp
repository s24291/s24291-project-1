#include <algorithm>
#include <iostream>
#include <iterator>
#include <stack>
#include <stdexcept>
#include <string>
#include <vector>
#include <cmath>

using namespace std;

auto pop_top(stack<double>& stack) -> double
{
    if (stack.empty()) {
        throw logic_error{"empty stack"};
    }
    auto element = move(stack.top());
    stack.pop();
    return element;
}

auto evaluate_addition(stack<double>& stack) -> void
{
    if (stack.size() < 2) {
        throw logic_error{"not enough operands for +"};
    }
    auto const b = pop_top(stack);
    auto const a = pop_top(stack);
    stack.push(a + b);
}

auto evaluate_subtraction(stack<double>& stack) -> void
{
    if (stack.size() < 2) {
        throw logic_error{"not enough operands for -"};
    }
    auto const b = pop_top(stack);
    auto const a = pop_top(stack);
    stack.push(a - b);
}

auto evaluate_multiplication(stack<double>& stack) -> void
{
    if (stack.size() < 2) {
        throw logic_error{"not enough operands for *"};
    }
    auto const b = pop_top(stack);
    auto const a = pop_top(stack);
    stack.push(a * b);
}

auto evaluate_division(stack<double>& stack) -> void
{
    if (stack.size() < 2) {
        throw logic_error{"not enough operands for /"};
    }
    auto const b = pop_top(stack);
    auto const a = pop_top(stack);
    stack.push(a / b);
}

auto evaluate_modulo(stack<double>& stack) -> void
{
    if (stack.size() < 2) {
        throw logic_error{"not enough operands for %"};
    }
    int b = pop_top(stack);
    int a = pop_top(stack);
    stack.push(a%b);
}


auto evaluate_exponentiation(stack<double>& stack) -> void
{
    if (stack.size() < 2) {
        throw logic_error{"not enough operands for **"};
    }
    auto const b = pop_top(stack);
    auto const a = pop_top(stack);
    stack.push(pow(a,b));
}

auto evaluate_square_root(stack<double>& stack) -> void
{
    if (stack.size() < 2) {
        throw logic_error{"not enough operands for sqrt"};
    }
    auto const b = pop_top(stack);
    auto const a = pop_top(stack);
    stack.push(sqrt(a));
    stack.push(sqrt(b));
}

auto evaluate_addition_with_substraction(stack<double>& stack) -> void
{
    if (stack.size() < 2) {
        throw logic_error{"not enough operands for + and -"};
    }
    auto const c = pop_top(stack);
    auto const b = pop_top(stack);
    auto const a = pop_top(stack);
    stack.push(a + b - c);
}


auto make_args(int argc, char* argv[]) -> vector<string>
{
    auto args = vector<string>{};
    copy_n(argv, argc, back_inserter(args));
    return args;
}



auto main(int argc, char* argv[]) -> int
{
    auto const args = make_args(argc - 1, argv + 1);

    auto stack = std::stack<double>{};
    for (auto const each : args) {
        try {
            if (each == "p") {
                cout << pop_top(stack) << "\n";
            } else if (each == "+") {
                evaluate_addition(stack);
            } else if (each == "-") {
                evaluate_subtraction(stack);
            } else if (each == "*") {
                evaluate_multiplication(stack);
            } else if (each == "/") {
                evaluate_division(stack);
            } else if (each == "%") {
                evaluate_modulo(stack);
            } else if (each == "**") {
                evaluate_exponentiation(stack);
            } else if (each == "sqrt") {
                evaluate_square_root(stack);
            } else if (each == "+,-") {
                evaluate_addition_with_substraction(stack);
            }
        } catch (logic_error const& e) {
            cerr << "error: " << each << ": " << e.what() << "\n";
        }
    }
    
        return 0;
}
