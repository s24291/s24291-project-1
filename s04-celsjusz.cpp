#include <iostream>
#include <string>
using namespace std;

struct Celsius 
{
    float temperature;
    string C;
    auto to_string() const -> string;
};

auto main (int argc, char*argv[]) -> int
{
    auto const x = stof(argv[1]);
    Celsius s;
    s.temperature= x;
    s.C= "°C";

    if(x <= -273.15)
    {
        cout << "-273.15" << s.C << "\n" << endl;
    }
    else
    {
        cout << s.temperature << s.C << "\n" << endl;
    }

return 0;
}
