#include <iostream>
#include <cstdlib>
using namespace std;

void conversion(int x)
{
    int i = 0;
    int tab[31];    
    while(x)
    {
        tab[i++] = x%7;
        x/= 7;
    }
    
    for (int j = i-1; j>= 0; j--)
    cout << tab[j];
}

auto main (int argc, char*argv[]) -> int
{
    auto const x = stoi(argv[1]);
    
    if (x<= 0)
    {
        cout << "the number needs to be bigger than 0" << "\n" << endl;
    }
    else
    {
        conversion(x);
        cout << "\n";
    }

return 0;
}
