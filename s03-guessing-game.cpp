#include <iostream>
using namespace std;

int main()
{
    int num, guess = 0;
    cout << "guessing game" << endl;
    srand(time(NULL)); 
    num = rand() % 100 + 1;
    
    
    do
    {
        cout << "guess:";
        cin >> guess;

        if (guess > num)
            cout << "too big!" << endl;
        else if (guess < num)
            cout << "too small!" << endl;
        else if (guess == num)
            cout << "just right!" << endl;
    } 
    while (guess != num);

    return 0;
}

