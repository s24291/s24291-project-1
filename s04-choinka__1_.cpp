#include <iostream>
#include <string>
using namespace std;

auto main (int argc, char*argv[]) -> int
{
    auto const a= stoi(argv[1]);
    
    for (int i = 1; i <= a; i++)
    {
        for (int j = 1; j <= a - i; j++)
            cout << " ";
        for (int j = 1; j <= i * 2 - 1; j++)
            cout << "*";

    cout << "\n" << endl;
    }

return 0;
}


